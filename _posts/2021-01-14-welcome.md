---
title: FullStackLive has its own website now!
date: Thu Jan 14 17:30:18 UTC 2021
category: Updates
tags: [training]
---

After a few weeks of a hiatus over the end of 2020, I've started to do live
coding again. I took my time off Twitch to think of a new strategy for my
stream. I'd like to do something that is new for my stream, and at the same
time something I feel well-equipped for. That's why I've decided to do an
experiment: I'd like to run courses live on Twitch!

Since my stream already was mostly about Ruby coding, I'm going to start
with a Ruby course. The [Courses page](/tabs/courses) has more about my
approach.

I'm happy to be back with FullStackLive and am looking forward to what 2021
is going to bring!
