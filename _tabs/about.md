---
title: About
icon: fas fa-info
order: 1
---

Welcome to FullStackLive.com! My name is Jochen, and I'm doing a regular
live coding stream called
"[Full Stack Live](https://www.twitch.tv/fullstacklive)" on Twitch.
