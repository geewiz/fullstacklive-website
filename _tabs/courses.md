---
title: Courses
icon: fas fa-tags
order: 2
---

This year, I'm going to try something new for my stream: I'm going to offer
courses live on Twitch!

I'd like to run these courses in a similar way to a book club. Every week,
I'm going to work through one chapter of the course material. Additionally
to my commentary, I'll also answer questions from the participants. This
means that, ideally, partipants come prepared by having worked through the
respective chapter in advance of the stream.

We're going to start with a Ruby course since that's the programming
language I've been using the most in the last 10 years.

## The FullStackLive Ruby Course

**The course start date is still TBD.**

For this course, I'm going to use the book
[The Well-Grounded Rubyist](https://www.manning.com/books/the-well-grounded-rubyist)
as the course material. Simply get it from my link or from where ever you get your literature.

[Follow me on Twitch](https://www.twitch.tv/fullstacklive) to stay up to
date on the course start date!
